// JavaScript Document


(function(){
	"use strict";
	
	var drawer = $(".sidebar-drawer"),
		windowWidth = $( window ).width(),
		sidebarPanel = $(".sidebar-panel"),
		mainbody = $(".mainbody"),
		burger = $(".burger"),
		headerMenu = $(".header-menu"),
		body = $("body"),
		detailInfoHeight = $(".job-detail-info").outerHeight(),
		locMap = $(".loc-map"),
		count = 0,
		counter = 0;
	
	drawer.on('click', function(){
		if(count === 0){
			drawer.addClass("drawer-slide");
			sidebarPanel.addClass("sidebar-slide");
			mainbody.addClass("mainbody-slide");
			setTimeout(function(){ mainbody.addClass("no-scroll"); }, 100);
			body.addClass("no-scroll");
			count = 1;
		} else {
			drawer.removeClass("drawer-slide");
			sidebarPanel.removeClass("sidebar-slide");
			mainbody.removeClass("mainbody-slide");
			setTimeout(function(){
				mainbody.removeClass("no-scroll");
			}, 100);
			setTimeout(function(){
				body.removeClass("no-scroll");
			}, 1000);
			count = 0;
		}
	});
	
	burger.on('click', function(){
		if(counter === 0){
			headerMenu.addClass("header-menu-slide");
			counter = 1;
		} else {
			headerMenu.removeClass("header-menu-slide");
			counter = 0;
		}
	});
	
	if(windowWidth > 1199){
		locMap.css('height', detailInfoHeight);
		$(window).resize(function(){
			var detailInfoHeight = $(".job-detail-info").outerHeight();
			locMap.css('height', detailInfoHeight);
		});
	} else {
		locMap.css('height', 300);
	}

//	$(window).resize(function(){
////		alert("yes");
////		alert(windowWidth);
//		if(windowWidth < 1199){
//		alert("yes");
//		var detailInfoHeight = $(".job-detail-info").outerHeight();
//			locMap.css('height', detailInfoHeight);
//		} else {
//			alert("no");
//			locMap.css('height', 300);
//		}
//	});
	
}());